<ul>
  <li>
    <a href="admin.php">
      <i class="glyphicon glyphicon-home"></i>
      <span>Dashboard</span>
    </a>
  </li>
  <li>
    <a href="#" class="submenu-toggle">
      <i class="glyphicon glyphicon-user"></i>
      <span>Usuarios</span>
    </a>
    <ul class="nav submenu">
      <li><a href="group.php">Administrar Grupos</a> </li>
      <li><a href="users.php">Administrar Usuarios</a> </li>
   </ul>
  </li>
  <li>
    <a href="categorie.php" >
      <i class="glyphicon glyphicon-indent-left"></i>
      <span>Categorias</span>
    </a>
  </li>
  <li>
    <a href="product.php" class="submenu-toggle">
      <i class="glyphicon glyphicon-book"></i>
      <span>Libros</span>
    </a>
    <!--<ul class="nav submenu">
       <li><a href="product.php">Manage products</a> </li>
       <li><a href="add_product.php">Add product</a> </li>
    </ul>-->
  </li>
  <li>
    <a href="media.php" >
      <i class="glyphicon glyphicon-picture"></i>
      <span>Meadias</span>
    </a>
  </li>
  <li>
    <a href="sales.php" class="submenu-toggle">
      <i class="glyphicon glyphicon-th-list"></i>
       <span>Préstamos</span>
      </a>
    <!--<ul class="nav submenu">
         <li><a href="sales.php">Manage Sales</a> </li>
         <li><a href="add_sale.php">Add Sale</a> </li>
     </ul>-->
  </li>
  <li>
    <a href="#" class="submenu-toggle">
      <i class="glyphicon glyphicon-stats"></i>
       <span>Reportes</span>
      </a>
      <ul class="nav submenu">
        <li><a href="sales_report.php">Reportes de Prestamos </a></li>
        <li><a href="monthly_sales.php">Reporte Mensual</a></li>
        <li><a href="daily_sales.php">Reporte Diario</a> </li>
      </ul>
  </li>
</ul>
